(setq user-emacs-directory "~/agam/projects/blogs/vanangamudi.gitlab.io/.emacs")   
(require 'package)
(package-initialize)
;;(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;;(package-refresh-contents)
;;(package-install 'org-plus-contrib)
(package-install 'htmlize)
(setq debug-on-error t)

;;; important otherwise ox-rss won't load
(add-to-list 'load-path "lisp/")   

(require 'org)
(require 'ox-publish)
(require 'ox-rss)

(setq user-full-name "வணங்காமுடி (vanangamudi)")

;;; treat .webp files as images
(setq org-html-inline-image-rules
      '(("file" . "\\(?:\\.\\(?:gif\\|\\(?:jpe?\\|pn\\|sv\\)g\\|webp\\)\\)")
        ("http" . "\\(?:\\.\\(?:gif\\|\\(?:jpe?\\|pn\\|sv\\)g\\|webp\\)\\)")
        ("https" . "\\(?:\\.\\(?:gif\\|\\(?:jpe?\\|pn\\|sv\\)g\\|webp\\)\\)")) )


(setq org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-toc nil)

(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-validation-link nil
      org-html-doctype "html5")

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg" "webp"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")

(setq org-publish-timestamp-directory "org-timestamps/")

(defconst html-posts-head "<link rel=\"stylesheet\" type=\"text/css\" href=\"/../static/css/style.css\" />")

(defconst html-preamble "<a href=\"/\">Home</a>")
(defconst html-posts-preamble
  ;;;TODO rss file name should be rss.xml 
  (concat html-preamble "
<a href=\"/posts.html\">Blog</a>
<a href=\"/posts/index.xml\">  Feed
  <img id=\"rss-logo\" src=\"../static/images/rss-logo.png\" width=\"16\" height=\"16\">
</a>
<script data-isso=\"/isso-comments\" src=\"/isso-comments/js/embed.min.js\"></script>
" ))

(defconst html-posts-postamble "<section id=\"isso-thread\"></section>")

(setq user-home "/home/vanangamudi/"
      pori-dir "public/pori")

(setq org-publish-project-alist
      ;; note that this line begins with a back tick not single quote
      ;; and there is no single quoting inside this block
      `(("pori"
          ;;; order of the components do not matter, but the list that follow this must be ordered
         :components ("agam" "pori-posts" "pori-static" "pori-rss"))

        ("pori-posts"
         :base-directory "~/agam/projects/blogs/vanangamudi.gitlab.io/posts/"
         :base-extension "org"
         :recursive t

         :exclude "README\\|TODO\\|draft\\|kuri\\|template"

         :publishing-function org-html-publish-to-html
         :publishing-directory  "~/public/pori/posts"

         :auto-sitemap t
         :sitemap-filename "index.org"
         :sitemap-file-entry-format "%d *%t*"
         :sitemap-sort-files  anti-chronologically
         :sitemap-title "கட்டுரைகள் (Articles)"

         :sitemap-style list
         :sitemap-sort-files anti-chronologically
         :html-head ,html-posts-head
         :html-preamble ,html-posts-preamble
         :html-postamble ,html-posts-postamble
         :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.ico\"/>")

        ("pori-static"
         :base-directory "~/agam/projects/blogs/vanangamudi.gitlab.io/static"
         :base-extension ,site-attachments

         :recursive t
         :publishing-directory "~/public/pori/static/"
         :publishing-function org-publish-attachment)

        ("pori-rss"
         :base-directory "~/agam/projects/blogs/vanangamudi.gitlab.io/posts"
         :base-extension "org"
         :recursive t

         :publishing-directory "~/public/pori/posts"
         :publishing-function (org-rss-publish-to-rss)

         :rss-extension "xml"
         :html-link-home "https://pori.vanangamudi.org/"
         :html-link-use-abs-url t
         :html-link-org-files-as-html t

         :section-numbers nil
         :exclude ".*"            ;; To exclude all files...
         ;;; () are important ("index.org")
         ;;; otheriwse we end with error
         ;;; Debugger entered--Lisp error: (wrong-type-argument stringp 105)
         ;;; expand-file-name(105 "~/agam/projects/blogs/vanangamudi.gitlab.io/posts/")
         ;;;
         :include ("index.org")  ;; ... except index.org.
         :table-of-contents nil
         )

        ;;; must last because the posts section generates
        ;;; sitemap into posts.org under this directory and
        ;;; for the main index.org to include posts.org this has to run last
        ("agam"
         :base-directory "~/agam/projects/blogs/vanangamudi.gitlab.io"
         :base-extension "org"

         :exclude "README\\|TODO\\|draft\\|kuri\\|template\\|server-config"
         ;; apparently the following do not work for :exclude
         ;; "\\(:?README\\|TODO\\|draft\\|kuri\\|template\\)"
         ;;,(regexp-opt '("kuri" "TODO" "README" "draft" "template"))

         :publishing-function org-html-publish-to-html
         :publishing-directory  "~/public/pori/"

         :auto-sitemap t
         :sitemap-filename "index.org"
         :sitemap-file-entry-format "%d *%t*"
         :sitemap-sort-files  anti-chronologically
         :sitemap-title "அகம்"

         :sitemap-style list
         :sitemap-sort-files anti-chronologically

         :html-head ,html-posts-head
         :html-preamble ,html-posts-preamble
         :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"/favicon.ico\"/>")

        ))

(provide 'publish)
;;; publish.el ends here
