#+SETUPFILE: ../templates/post.org
#+TITLE: Programmable Function Generator in C
#+author: vanangamudi
#+date: <29-02-2012>

#+BEGIN_SRC c

#include<reg51.h>
#include<string.h>

typedef unsigned char uchar;
uchar flag;
uchar count=0;
uchar cmd[10];
uchar sineLukup[]={    0x80
                       ,0x96
                       ,0xab
                       ,0xc0
                       ,0xd2
                       ,0xe2
                       ,0xef
                       ,0xf8
                       ,0xfe
                       ,0xff
                       ,0xfe
                       ,0xf8
                       ,0xef
                       ,0xe2
                       ,0xd2
                       ,0xc0
                       ,0xab
                       ,0x96
                       ,0x80
                       ,0x6a
                       ,0x54
                       ,0x40
                       ,0x2e
                       ,0x1e
                       ,0x11
                       ,0x80
                       ,0x20
                       ,0x00
                       ,0x20
                       ,0x80
                       ,0x11
                       ,0x1e
                       ,0x2e
                       ,0x40
                       ,0x54
                       ,0x6a
                       ,0x80};

code uchar rIDN[] = "PROGRAMMABLE FUCNTION GENERATOR";
code uchar rVER[] = "version 1.1";
code uchar rMODSET[] = "MODE SET SUCCESSFUL";
code uchar rCMDERR[]="CMDERR";

void sendResp(uchar *val,uchar cnt);

void sqrWav();
void sthWav();
void sinWav();
void triWav();

void (*wave)();

void delay01ms(uchar val);

void initProgram()
{
  EA=1;
  ES=1;

  SCON =0x50;
  TMOD=0x21;
  TH1 = 0x253;
  TL1 = 0X253;
  TR1 =1;

  flag=1;

}
void sqrWav()
{
  while( flag ){
    P0 = !P0;
    delay01ms(1);
  }
}


void delay01ms(uchar val)
{
  uchar i;
  for(i=0;i<val;i++){
    TH0=0x22;
    TL0=0x00;
    TF0=0;
    TR0=1;
    while(TF0!=1);
  }


}

void sthWav()
{    uchar i;
  while(flag){
    P0=i++;
  }
}


void sinWav()
{    uchar i=0;
  while(flag){
    for(i=0;i<37  ;i++){
      P0=sineLukup[i];
      delay01ms(1);
    }
  }
}


void triWav()
{    uchar i,TEMP;
  TI=0;
  while(flag){
    TEMP = 0;
    for(i=0;i<200;i++)
      P0=TEMP++;

    TEMP = 200 ;
    for(i=0;i<200;i++)
      P0=TEMP--;

  }
}

void processQry()
{
  if(!strncmp(cmd,"*IDN?",5)){
    sendResp(rIDN,31);
  }
  else if(!strncmp(cmd,":VER?",5)){
    sendResp(rVER,10);
  }
  else if(!strncmp(cmd,":MOD:SIN",8)){
    sendResp(rMODSET,19);
    wave = sinWav;
    flag=0;
  }
  else if(!strncmp(cmd,":MOD:SQR",8)){
    sendResp(rMODSET,19);
    wave = sqrWav;
    flag=0;
  }
  else if(!strncmp(cmd,":MOD:STH",8)){
    sendResp(rMODSET,19);
    wave = sthWav;
    flag=0;
  }
  else if(!strncmp(cmd,":MOD:TRI",8)){
    sendResp(rMODSET,19);
    wave = triWav;
    flag=0;
  }
  else{
    sendResp(rCMDERR,6);
  }

}

void sendSerial(uchar val)
{
  TI=0;
  SBUF = val;
  while(TI!=1);
}

void sendResp(uchar *val, uchar cnt)
{
  uchar i=0;
  for(i=0;i<cnt;i++){
    sendSerial(val[i]);
    delay01ms(1);
  }
  TI=0;
}

void serialISR(void) interrupt 4
{
  if(RI==1)
    {
      RI=0;
      cmd[count]=SBUF;
      count++;

      if(SBUF==13){
        processQry();
        count=0;
      }
    }
}

void main()
{
  initProgram();
  wave=sqrWav;
  while(1){
    EA=1;
    ES=1;
    flag=1;
    wave();
  }

}
#+END_SRC
