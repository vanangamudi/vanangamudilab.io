#+SETUPFILE: ../templates/post.org
#+SETUPFILE: ../templates/post.org
#+TITLE:  Accelerometers
#+author: vanangamudi
#+date: <24-10-2011>

 Acceleration is measured by the accelerometers. Acceleration means not only w.r.t positional change. acceleration is simply force per unit mass.

    F=m.a ===> a = F/m .


For you to have some interest in accelerometers, I'll give you some real-time applications...

Mobile phones: accelerometers are used in iphone, ipads, N96, N85, even G-five mobiles for orientation detection. When you tilt the phone you may have noticed the display changes from portrait to landcape and vice versa. this is possible with the help of accelerometers.

Camera image stabilization: the still camera, camcorders uses accelerometers for image stabilization to  avoid motion blurring. other application include, Earth quake survey, Explosion tests.

Game controllers: Steering based controllers used in gaming has accelerometers for realistics game experience.

Weird things

    Accelerations below 0.001 m/s² are measured in seismic surveys.
    A racing car driver can experience 50 m/s². Most humans lose consciousness at around 60 m/s².
    A car accident of 100 m/s² will break human bones while 300 m/s² are sufficient for a seatbelt to break ribs.
    A laptop dropping onto a concrete floor from a height of 1 m may endure as much as 20,000 m/s².
    Accelerations beyond 100,000 m/s² are found in ballistics and explosion tests.


General structure:
      It consists of a vibrating element. This element vibrates in proportion with the acceleration of body of interest. usually the accelerometer is mounted on the body whose acceleration/vibration to be measured.

The figure below shows the mathematical model of accelerometer

[[file:../static/images/accelerometer.jpg]]
 
