#+SETUPFILE: ../templates/post.org
#+TITLE: Marumugam: Power Loss Indication
#+author: vanangamudi
#+date: <18-10-2011>



[[file:../static/images/marumugam-pli-circuit.webp]]

 Actually this stuff is made for a Simple(labs) competition. Problem statement is to indicate the power loss by blinking an LED after the power goes off. We thought of storing the charge in capacitor, like everyone. It seem simple at first, complex then, finally pretty easy.

    Operation is simple. When power is available the Capacitor is charged through the PN Junction Diode(1N4007). The transistor is kept ON by the power supply. When the power goes OFF, the Capacitor drives the 555 timer - Blinking Circuitry to indicate that the power is lost. Click here to see 555 timer Basics

Here is the circuit.

[[file:../static/images/marumugam-power-loss-detection-2.webp]]

Operation Simplified....

[[file:../static/images/marumugam-powerloss.gif]]

   Actually the 555 timer circuitry operates in Astable Mutlivibrator Mode to generate a pulse. The pulse is used to drive the LED - On/Off. The supply to the 555 is supplied by the Charge Retention Capacitor (array). It is nothing but a ordinary capacitor of high capacitance value. Diode is used to prevent the transistor to be biased from capacitor. The transistor goes Off with the power, so the charge from Capacitor is used to run 555 timer circuitry.


[[file:../static/images/marumugam-pli-breadboard.webp]]
